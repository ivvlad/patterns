import { CARS, COMMENTS_TO_USER, START_GAME_COMMENT, WELCOME_COMMENTS } from "../commentsData";
import { getRandomIndex } from "../helpers";
import { IntermediateMessage, introduceUsers } from "../helpers/comments";
import { COMMENT, ERROR } from "../socketEnv";

// Factory pattern
class Commentator {
  constructor(socket) {
    this.socket = socket;
  }

  sendToRoom(action, ...args) {
    this.socket.emit(action, ...args);
    this.socket.broadcast.to(this.roomId).emit(action, ...args);
  }

  onGameStart(roomId) {
    this.roomId = roomId;

    this.sendToRoom(COMMENT, START_GAME_COMMENT);
  }

  onIntroduceUsers(room) {
    const users = Object.entries(room).map(userData => userData[0]);
    const index = getRandomIndex(WELCOME_COMMENTS.length);

    this.sendToRoom(COMMENT, WELCOME_COMMENTS[index]);

    introduceUsers(this.roomId, [...CARS], [...users], users.length, this.socket);
  }

  onGap(user) {
    const index = getRandomIndex(COMMENTS_TO_USER.length);

    this.sendToRoom(COMMENT, `${user}${COMMENTS_TO_USER[index]}`);
  }

  onIntermediateResult(inGame) {
    try {
      const userResults = 
      Object
        .entries(inGame[this.roomId])
        .filter(entries => typeof entries[1] === 'object' && !Array.isArray(entries[1]))
        .map(user => [ user[0], user[1]['percent']]);

    const userRating = userResults.sort((a, b) => b[1] - a[1]);
    const message = IntermediateMessage(userRating);

    this.sendToRoom(COMMENT, message);
    } catch (err) {
      console.warn('There seems to be no user left in the room.')
    }
  }

  onNearTheFinish(username, nearTheFinish) {
    let message = `${username} уже приближается к финишу. Посмотрим, успеет ли кто-то вырваться вперед или этот заезд остается за ним.`;

    if (nearTheFinish.length) {
      message = `Следом за ${nearTheFinish.join(', ')}, ${username} приближается к финишной черте.`
    }
    nearTheFinish.push(username);

    this.sendToRoom(COMMENT, message);
  }

  onFinish(username) {
    this.sendToRoom(COMMENT, `${username} пересекает финишную черту.`);
  }

  onGameOver(results, allTime) {
    try {
      const [ firstUser, secondUser, thirdUser ] = results;
      let message = 
        `Первое место занимает ${firstUser[0]}, для победы ему потребовалось всего ${allTime - firstUser[1]} секунд`;
      
      if (secondUser) {
        message = message + `, за ним идет ${secondUser[0]}, который справился за ${allTime - secondUser[1]} секунд`;
  
        if (thirdUser) {
          message = message + `и третье место занимает ${thirdUser[0]}, которому для преодаления финишной черты потребовалось ${allTime - thirdUser[1]} секунд`;
        }
      }
  
      this.sendToRoom(COMMENT, `${message}.`);
    } catch (err) {
      this.sendToRoom(ERROR, 'There was a technical problem, the user decided to leave us...')
    }
  }
}

export default Commentator;