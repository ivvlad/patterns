import { texts } from "../data";
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from "../socket/config";
import { GAME_OVER, GAME_STARTED, ROOMS, USERS_UPDATED } from "../socketEnv";

export const isRoomAvailable = (username, roomId, room, inGame) => room && !room[username] && !inGame[roomId] && getObjectLength(room) < MAXIMUM_USERS_FOR_ONE_ROOM;

export const leaveFromRooms = (user, socket) => {
  user.forEach(room => socket.leave(room));
  user = [];
}

export const isUserExist = (place, roomId, username) => place[roomId] && place[roomId][username];

export const deleteInGameData = (inGame, roomId, username) => {
  if (getObjectLength(inGame[roomId] <= 3)) {
    delete inGame[roomId];
  } else {
    delete inGame[roomId][username];
  }
}

export const getObjectLength = (object) =>  {
  let length = 0;

  for (const key in object) {
    if (object.hasOwnProperty(key)) {
      length += 1;
    };
  }
  return length;
};

export const getRandomIndex = (max) => Math.floor(Math.random() * max);

const createInGameRoom = (roomId, inGame,) => {
  inGame[roomId] = {
    roomId,
    nearFinish: [],
  }
};

const updateEntries = (entries, roomId, inGame) => {
  entries.forEach(user => {
    inGame[roomId] = {
      ...inGame[roomId],
      [user[0]]: { percent: 0 },
    };
  })
}

const onIntroduce = (room, commentator) => setTimeout(() => {
  commentator.onIntroduceUsers(room);
}, 2000);

const onGap = (entries, seconds, commentator) => setTimeout(() => {
  const index = getRandomIndex(entries.length);

  commentator.onGap(entries[index][0]);
}, (SECONDS_TIMER_BEFORE_START_GAME + seconds) * 1000);

const onIntermediate = (inGame, seconds, commentator) => setTimeout(() => {
  commentator.onIntermediateResult(inGame);
}, (SECONDS_TIMER_BEFORE_START_GAME + seconds) * 1000) // when game starts, after 30 sec

// higher-order function
const repeat = (entries, commentator, times, delay, callback) => {
  let counter;

  const interval = setInterval(() => {
    counter += 1;
    callback(entries, delay, commentator);

    if (counter === times) {
      clearInterval(interval);
    }
  }, delay * 1000)
};

export const updateUsersStatus = (roomId, rooms, inGame, commentator, socket, onLeave = false) => {
  const room = rooms[roomId];
  const roomEntries = Object.entries(room);
  const allAreReady = roomEntries.every(el => el[1] === 'ready');

  if (!onLeave) {
    socket.emit(USERS_UPDATED, room);
    socket.to(roomId).emit(USERS_UPDATED, room);
  }

  if (allAreReady) {
    const textId = getRandomIndex(texts.length);

    createInGameRoom(roomId, inGame);
    updateEntries(roomEntries, roomId, inGame);
    
    commentator.onGameStart(roomId);

    onIntroduce(room, commentator);
    onIntermediate(inGame, 30, commentator);
    repeat(roomEntries, commentator, 1, 12, onGap);

    socket.broadcast.emit(ROOMS, rooms);
    socket.emit(GAME_STARTED, roomId, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, textId);
    socket.broadcast.to(roomId).emit(GAME_STARTED, roomId, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, textId);
  }
}

const checkIsOver = (roomUsers, inGameUsers, deletedUser) => inGameUsers.every(user => {
  if (user[0] !== deletedUser && roomUsers.includes(user[0])) {
    return user[1]["percent"] === 100; // true if game end, false if game in process
  }

  return true;
});

const getResults = (inGameUsers) => {
  const formattedUsers = inGameUsers.map(user => [user[0], user[1]['time']]);
  const finishedUsers = formattedUsers.filter(user => typeof user[1] === 'number');

  return finishedUsers.length > 1 && finishedUsers.sort((a, b) => b[1] - a[1]) || finishedUsers;
}

const resetUsers = (room) => {
  const updatedUsers = Object.entries(room).map(el => [el[0], 'not-ready']);
  
  return Object.fromEntries(updatedUsers);
}

export const updateInGameUsers = (users) => {
  const usersEntries = Object.entries(users);
  const updatedUsers = usersEntries.map((user, index) => {
    if (index < 2) {
      return user; // room name && finish data
    }

    return [user[0], user[1]['percent'] < 100 ? { ...user[1], time: 0 } : user[1]];
  });

  return Object.fromEntries(updatedUsers);
}

export const checkIsWin = ({roomId, rooms, inGame, commentator, socket, deletedUser = '', isGameOver = false}) => {
  if (rooms[roomId] && inGame[roomId]) {
    const roomUsers = Object.entries(rooms[roomId]).map(user => user[0]);
    const inGameEntries = Object.entries(inGame[roomId]).filter(entries => typeof entries[1] === 'object' && !Array.isArray(entries[1]))

    const isOver = checkIsOver(roomUsers, inGameEntries, deletedUser);
  
    if (isOver || isGameOver) {
      inGame[roomId] = undefined;
      rooms[roomId] = resetUsers(rooms[roomId]);

      const userResults = getResults(inGameEntries);

      commentator.onGameOver(userResults, SECONDS_FOR_GAME);

      socket.emit(GAME_OVER, userResults, SECONDS_FOR_GAME);
      socket.to(roomId).broadcast.emit(GAME_OVER, userResults, SECONDS_FOR_GAME);
    }
  }
}