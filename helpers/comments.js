import { getRandomIndex } from ".";
import { COMMENT } from "../socketEnv";

export const IntermediateMessage = (rating) => {
  const users = rating.map(user => user[0]);
  let message = `30 секунд на таймере и это значит, что половина времени уже прошла, первое место удерживает ${users[0]}`;

  rating.forEach((data, index) => {
    const [user, position] = data;

    if (index !== 0) {
      message = message + ` за ним следует ${user}, отставая на ${rating[index - 1][1] - position}%`
    }
  });

  return message + '. Посмотрим, как события пойдут дальше.';
}

export const introduceUsers = (roomId, cars, users, usersLenth, socket) => {
  let intervalCounter = 0;

  const usersInterval = setInterval(() => {
    const userIndex = getRandomIndex(cars.length);
    const user = users.shift();
    const car = cars[userIndex];

    intervalCounter += 1;
    cars = cars.filter(el => el !== car);

    socket.emit(COMMENT, `${user} под номером ${intervalCounter}${car}`);
    socket.broadcast.to(roomId).emit(COMMENT, `${user} под номером ${intervalCounter}${car}`);

    if (intervalCounter === usersLenth) {
      clearInterval(usersInterval);
    }
  }, 2000)
}